<?php
class db
{
    private $dbHost = 'localhost';
    private $dbUser = 'root';
    private $dbPassword = '12345678';
    private $dbName = 'compraclickbd';
    private $dbConexion;



    function __construct()
    {
        try {
            $mysqlConnect = 'mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName;
            $this->dbConexion = new PDO($mysqlConnect, $this->dbUser, $this->dbPassword);
            return $this->dbConexion;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }


    public function getAutenticarUsuario($infoAuth)
    {
        $query = $this->dbConexion->prepare("select * from usuario where email=:email and password=:pass");

        $query->execute([
            'email' => $infoAuth['email'],
            'pass' => $infoAuth['pass']
        ]);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        // $jsonResponse = json_encode($results);

        if (count($results)) {
            $res = "Existe";
        } else {
            $res = "No existe";
        }
        return ['estatus'=>$res];
    }

    public function getAllUsers()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM usuario");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        $jsonResponse = json_encode($results);
        return $results;
    }
    public function getUsersCompradores()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM usuario WHERE idTipoUser=1");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        $jsonResponse = json_encode($results);
        return $results;
    }
    public function getUsersVendedores()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM usuario WHERE idTipoUser=2");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        $jsonResponse = json_encode($results);
        return $results;
    }
    public function getAllDirecciones()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM direccion");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        $jsonResponse = json_encode($results);
        return $results;
    }

    public function getProductoById($id){
        $query = $this->dbConexion->prepare("SELECT * FROM producto WHERE idProducto =:id");
        $query->execute(['id' =>$id]);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function getAllProductos()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM inventario");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function getAllCompras()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM compra");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
    public function getAllPedidos()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM pedido");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
    public function getAllEnvios()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM envio");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
    public function getAllEstatus()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM estatus");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
    public function getAllVentas()
    {
        $query = $this->dbConexion->prepare("SELECT * FROM venta");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function addDireccion($descripcion)
    {
        $query = $this->dbConexion->prepare("INSERT INTO direccion (descripcion) VALUES (:descripcion)");
        $query->execute(['descripcion' => $descripcion['descripcion']]);
        // $results = $query->fetchAll(PDO::FETCH_ASSOC);
        // $jsonResponse = json_encode($results);
        
        $q = $this->dbConexion->prepare("SELECT idDireccion FROM direccion ORDER BY idDireccion DESC LIMIT 1");
        $q->execute();
        $results = $q->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }



    public function addCompra($compraInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO compra(idProducto, idUserComprador, fechaCompra) VALUES(:idProducto, :idUserComprador, :fechaCompra)');
        $query->execute([
            'idProducto' => $compraInfo['idProducto'],
            'idUserComprador' => $compraInfo['idUserComprador'],
            'fechaCompra' => $compraInfo['fechaCompra']
        ]);
        return $query;
    }

    public function addPedido($pedidoInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO pedido(idCompra, descripcion, precioTotal) VALUES(:idCompra, :descripcion, :precioTotal)');
        $query->execute([
            'idCompra' => $pedidoInfo['idCompra'],
            'descripcion' => $pedidoInfo['descripcion'],
            'precioTotal' => $pedidoInfo['precioTotal']
        ]);
        return $query;
    }

    public function addEnvio($envioInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO envio(idPedido, fechaEstimada) VALUES(:idPedido, :fechaEstimada)');
        $query->execute([
            'idPedido' => $envioInfo['idPedido'],
            'fechaEstimada' => $envioInfo['fechaEstimada']
        ]);
        return $query;
    }
    public function addVenta($ventaInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO venta(idEnvio, idEstatus) VALUES(:idEnvio, :idEstatus)');
        $query->execute([
            'idEnvio' => $ventaInfo['idEnvio'],
            'idEstatus' => $ventaInfo['idEstatus']
        ]);
        return $query;
    }


    public function addUser($usuarioInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO usuario(nombre, apellidoP, apellidoM, idDireccion, telefono, email, password, clabe, idTipoUser) VALUES(:nombre, :apellidoP, :apellidoM, :idDireccion, :telefono, :email, :pass, :clabe, :idTipoUser)');

        $query->execute([
            'nombre' => $usuarioInfo['nombre'],
            'apellidoP' => $usuarioInfo['apellidoP'],
            'apellidoM' => $usuarioInfo['apellidoM'],
            'idDireccion' => $usuarioInfo['idDireccion'],
            'telefono' => $usuarioInfo['telefono'],
            'email' => $usuarioInfo['email'],
            'pass' => $usuarioInfo['password'],
            'clabe' => $usuarioInfo['clabe'],
            'idTipoUser' => $usuarioInfo['idTipoUser']

        ]);
        return $query;
    }



    public function addProducto($productoInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO producto(idUserVendedor, nombreProducto, descripcion, precio, imagenProducto) VALUES(:idVendedor,:nombre, :descripcion, :precio, :imagenProducto)');


        $query->execute([
            'idVendedor' => $productoInfo['idVendedor'],
            'nombre' => $productoInfo['nombre'],
            'descripcion' => $productoInfo['descripcion'],
            'precio' => $productoInfo['precio'],
            'imagenProducto' => $productoInfo['imagenProducto']
        ]);


        $query = $this->dbConexion->prepare("SELECT * FROM producto");
        $query->execute();



        $consulta = $this->dbConexion->prepare("SELECT idProducto FROM producto ORDER BY idProducto DESC LIMIT 1");
        $consulta->execute();
        $res = $consulta->fetch(PDO::FETCH_ASSOC);
        $info = array(
            'idProducto' => $res['idProducto'],
            'existencias' => $productoInfo['existencias']
        );
        //echo json_encode($info);
        $this->addInventario($info);
        return $query;
    }
    public function addInventario($productoInfo)
    {
        $query = $this->dbConexion->prepare('INSERT INTO inventario(idProducto, existencia) VALUES(:idProducto,:existencias)');

        $query->execute([
            'idProducto' => $productoInfo['idProducto'],
            'existencias' => $productoInfo['existencias']
        ]);

        return $query;
    }
}
