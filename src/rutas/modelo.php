<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../config/db.php';

$app = AppFactory::create();

$app->get('/login/autenticar', function (Request $request, Response $response, $args) {

    $result = $request->getQueryParams();
    $db = new db();
    $r = $db->getAutenticarUsuario($result);
    $response->getBody()->write(json_encode($r));
    return $response;
});


$app->get('/obtener/usuarios', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllUsers();
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->get('/obtener/vendedores', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getUsersVendedores();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/compradores', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getUsersCompradores();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/direcciones', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllDirecciones();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/productos', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllProductos();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/producto/{idPr}', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getProductoById($args['idPr']);
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/compras', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllCompras();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/pedidos', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllPedidos();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/envios', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllEnvios();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/ventas', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllVentas();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->get('/obtener/estatus', function (Request $request, Response $response, $args) {
    $db = new db();
    $result = $db->getAllEstatus();
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->post('/add/envio', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $db->addEnvio($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->post('/add/venta', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $db->addVenta($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});

$app->post('/add/direccion', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $res = $db->addDireccion($result);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->post('/add/usuario', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $db->addUser($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->post('/add/producto', function ($request, $response, array $args) {
    $result = $request->getParsedBody();



    $db = new db();
    $db->addProducto($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->post('/add/compra', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $db->addCompra($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->post('/add/pedido', function ($request, $response, array $args) {
    $result = $request->getParsedBody();
    $db = new db();
    $db->addPedido($result);
    $response->getBody()->write(json_encode($result));
    return $response;
});
